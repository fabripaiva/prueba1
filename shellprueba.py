#!/bin/python
from cmd import Cmd #Libreria para linea la de comandos
import os   # Libreria de utilidades de sistema operativo
import shutil  # Libreria de alto nivel para manejo de archivos
import subprocess #Libreria para manejo de subprocesos
import errno  # para manejo de errores
import sys # provee acceso a funciones mantenidas por el interprete(python)
from ftplib import FTP  # libreria para FTP(file transfer protocol )
import datetime  #manejo de formatos de fecha y fechas
import time   # manejo de fechas
import pathlib #libreria para manejo de directorios
pids = {}  # Se crea un diccionario (clave, valor) para manejar los subprocesos utilizando su pid (process id)
class MyPrompt(Cmd):
    usuario = 'default' #creamos una variable global usuario para poder almacenar el usuario que inicio sesion y asi poder imprimir en login y exit   
    prompt = 'SO$> '
    intro = "Bienvenido a la shell de FABRIZIO Y OLIVA, Escriba HELP para mas ayuda"
    #comando para registrar inicio de sesion y comparar si esta en su tiempo laboral
    def do_login(self, inp):
        inp = inp.split() #lo que entra en el teclado separa en cadenas de acuerdo a donde queremos llevar
        global usuario 
        encontrado=0
        noencontrado=1
        encontrado1=0
        noencontrado1=1
        usuario = inp[0]# queda guardado el nombre de usuario iniciado
        with open("/var/log/usuarios_log") as f:  #verificamos si el usuario existe en usuarios_log 
            for line in f: 
                if inp[0] in line:
                    encontrado = True
                else:
                    noencontrado= False
        print("Ingrese su contraseña: ")
        contras = input()            
        with open("/root/passwdusuarios_log") as f:  #verificamos si el usuario existe en usuarios_log 
            for line in f: 
                if contras in line:
                    encontrado1 = True
                else:
                    noencontrado1= False            
        if encontrado == True & encontrado1 == True:
            print("El usuario ha logueado correctamente")
        else:
            print("Error, verique si el usuario esta creado o verifique la contraseña")                                       
        with open('/var/log/usuarios_log') as f: #Mientras se ejecute todas las sentencias de abajo, este se guardara en usuario log
            for line in f:                         #en usuario_log contiene el usuario con sus horas de trabajos e ip
                if inp[0] in line:
                    us= line.split() #en us se guarda y luego separa la hora de entrada y salida, es el apuntador al usuario
                    now = datetime.datetime.now(); #en esta funcion guarda la hora local en now para comparar luego con la hora de salida y entrada
                    hora_entrada = datetime.datetime.strptime(us[1], '%H:%M') #transforma de cadena a objeto tipo fecha
                    hora_entrada = now.replace(hour=hora_entrada.time().hour, minute=hora_entrada.time().minute, second=hora_entrada.time().second, microsecond=0) #reemplaza la hora de datime por el objeto de arriba
                    hora_salida = datetime.datetime.strptime(us[2], '%H:%M')    #y aca la hora de salida
                    hora_salida =now.replace(hour=hora_salida.time().hour, minute=hora_salida.time().minute, second=hora_salida.time().second, microsecond=0)
                    sh = open("/var/log/personal_horarios_log", "a")
                    if hora_entrada <= now <= hora_salida:
                        user = str.format("Horario correcto, conectado desde IP:" + us[3] + ' usuario: ' + inp[0]) #esta en la hora de trabajo correcta
                        sh.write(now.strftime("%m/%d/%Y, %H:%M:%S") + " " + user + " " "\n") #escribe en el archivo los parametros
                        sh.close() #cierra archivo
                    else:
                        user = str.format("Fuera de horario, conectado desde IP:" + us[3] + ' usuario: ' + inp[0])#esta en la hora de trabajo incorrecta
                        sh.write(now.strftime("%m/%d/%Y, %H:%M:%S") + " " + user + " " "\n")    #escribe en el archivo los parametros
                        sh.close() #cierra archivo
    def logshell(self, com, inp):
        # uno de los requerimientos es que cada comando listado vaya a la carpeta log /var/log
        sh = open("/var/log/lfs_log", "a")  # abre el archivo en la carpeta /var/log
        hora = time.strftime("%y-%m-%d %H:%M:%S")  # capta el horario que se hizo el comando y lo guarda en hora
        sh.write(hora + " " + com + " " + inp + "\n")  # Escribe los parametros de fecha y hora
        sh.close()  # cierra el archivo -----> esto se repita en cada def, por el cual ya no esta mas escrito ya que es lo mismo
    def logshell_transferencias(self, inp):
        # uno de los requerimientos es que cada comando listado vaya a la carpeta log /var/log
        sh = open("/var/log/lfstransferencias_log", "a")  # abre el archivo en la carpeta /var/logtransferencias_log
        hora = time.strftime("%y-%m-%d %H:%M:%S")  # capta el horario que se hizo el comando y lo guarda en hora
        sh.write(hora + " " + inp + "\n")  # Escribe los parametros de fecha y hora
        sh.close()  # cierra el archivo -----> esto se repita en cada def, por el cual ya no esta mas escrito ya que es lo mismo   
    #aca se guardaran los nombres de los usuarios creados
    # comando renom un archivo rename
    def do_renom(self, inp):
        self.logshell("renom",inp)  # llamaa a la funcion logshell y escribe rename y lo escrito en teclado para saber los parametros
        inp = inp.split()  # este comando funciona igual que el comando move, ya que solamente emula moviendo
        try: #ejecuta el codigo luego de try y si explota va a except, y asi con todas las funciones
            # Mueve recursivamente el archivo a otra ubicacion que el usuarios nombra, simulando el cambio de nombre
            shutil.move(inp[0], inp[1])
        except: #permite manipulacion de los errores, se repite en cada comando
            print("El nombre no es valido o el archivo no existe")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("El nombre no es valido o el archivo no existe")
            sh.write(error + " " "\n")
            sh.close()
    # comando para moverse archivos
    def do_mover(self, inp):
        self.logshell("mover", inp)  # Separara las cadenas en n cadenas a traves de una llave recibiendo un string de por paso
        # recibe in string y separa las cadenas en n cadenas a traves de una llave
        inp = inp.split()
        try:
            if os.path.exists(inp[0]) & os.path.exists(inp[1]) :  
                ruta = shutil.move(inp[0], inp[1])
                print('El directorio ha sido movido a', ruta)
            else:
                print("Las rutas no son validas o el archivo no existe")
                sh = open("/var/log/errores_sistema_log", "a")
                error = str.format("Las rutas no son validas o el archivo no existe ")
                sh.write(error + " " "\n")
                sh.close()       
        except IndexError:
            print("Las rutas no son validas o el archivo no existe")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("Las rutas no son validas o el archivo no existe ")
            sh.write(error + " " "\n")
            sh.close()   
        except OSError as exc:
            print("No se ha podido copiar el archivo o el directorio, verificar sintaxis")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("No se ha podido copiar el archivo o el directorio, verificar sintaxis ")
            sh.write(error + " " "\n")
            sh.close()       
     # Comando Crear un directorio
    def do_crdir(self, inp):
        self.logshell("crdir", inp)
        inp = inp.split()  # lo que recibe por teclado hace un split (separar) y ordena cuando encuentra un espacio
        try:
            os.makedirs(inp[0])  #makedirs realiza una llamada al sistema de la funcion mkdir
        except:
            print("El archivo ya existe o la ruta ingresada no es valida")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("El archivo ya existe o la ruta ingresada no es valida")
            sh.write(error + " " "\n")
            sh.close()
    # Comando Cambiar los propietarios de un archivo o un grupo de archivos
    def do_cprop(self, inp):
        self.logshell("cprop", inp)
        inp = inp.split()
        try:
            shutil.chown(inp[0], inp[1], inp[2])  # Realiza una llamada el comando chown cambian propietario de la ruta dada
        except:
            print("No se pudo cambiar de propietario, error")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("No se pudo cambiar de propietario, error")
            sh.write(error + " " "\n")
            sh.close()
    # Comando Cambiar los permisos sobre un archivo o un grupo de archivos
    def do_cambiarper(self, inp):
        self.logshell("cambiarper", inp)
        inp = inp.split()
        try:
           inp[1] = int(inp[1], 8)  # colocando el 8 como segundo parametro hace que la cadena sea en octal, 777 640
           os.chmod(inp[0], inp[1])  # Realiza una llamada al sistema a la funcion chmod
        except:
            print("No se ha podido cambiar de permisos, verifique el nombre del archivo o los permisos")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("No se ha podido cambiar de permisos, verifique el nombre del archivo o los permisos")
            sh.write(error + " " "\n")
            sh.close()
    # Comando para cambiar la contraseña
    def do_cambpass(self, inp):
        self.logshell("cambpass", inp)
        inp = inp.split()
        try:
            print("Escriba primeramente su contraseña aqui: ")
            os.system("passwd " + inp[0])   # pasa la pregunta a la shell principal, realiza una llamada al sistema al passwd del SO
            sh =open("/root/passwdusuarios_log", "a")
            user2 = str.format(inp[1] + " ")
            sh.write(user2 + " " "\n")
            sh.close()
        except:
            print("No se ha podido cambiar la contrasena")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("No se ha podido cambiar la contrasena")
            sh.write(error + " " "\n")
            sh.close()
    # Comando cop
    # no se puede llamar al sistema
    def do_cop(self, inp):
        self.logshell("cop", inp)
        inp = inp.split()  #lee el archivo fuente completo en la memoria antes de volver a escribirlo. 
        try:
            if os.path.exists(inp[0]): 
                with open(inp[0], 'rb') as src, open(inp[1], 'wb') as dst: dst.write(src.read()) #sirve para agilizar memoria para todas las operaciones de copia de archivos, excepto las más pequeñas.
                print('El directorio ha sido copiado a', inp[1])
            else:
                print("Las rutas no son validas o el archivo no existe")
                sh = open("/var/log/errores_sistema_log", "a")
                error = str.format("Las rutas no son validas o el archivo no existe ")
                sh.write(error + " " "\n")
                sh.close()
        except OSError as exc:
            print("No se ha podido copiar el archivo o el directorio, verificar la sintaxis y directorio destino")
   
    def list_dir(self, dir):
        path = pathlib.Path(dir)
        dir = []
        try:
            for item in path.iterdir():    #Verifica si los directorios existen para luego ser llamada en la funcion de abajo
                if item.is_dir():
                    dir.append(str(item)) #si es el directorio entonces ejectuta la misma funcion hasta que no haya subdirectorios
                    dir = dir + self.list_dir(item)   #verifica cada archivo/item que tiene en el directorio para realizar el print 
                elif item.is_file():                #asi va guardando los pathsy y los nombres de los archivos
                    dir.append(str(item))
            return dir
        except FileNotFoundError:
            print('Invalid directory')

    # Comando Listar directorios.No se puede hacer una llamada al sistema
    def do_listar(self, inp):
        try:
            self.logshell("listar", inp)
            inp = inp.split()
            print(*self.list_dir(inp[0]), sep='\n') #imprime uno debajo de otro
        except:
            print("Por favor ingrese una ruta o la ruta ingresada no existe")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("Por favor ingrese una ruta o la ruta ingresada no existe ")
            sh.write(error + " " "\n")
            sh.close()
    # Comando para cambiar directorio (no se puede hacer una llamada al sistema de la funcion cd)
    def do_cambiardir(self, inp):
        self.logshell("cambiardir", inp)
        inp = inp.split()
        try:
            # La funcion cambia de directo
            os.chdir(inp[0])  # busca el directorio introducido por teclado y llama a os para ver si es correcto
            print(os.getcwd()) #imprime el directorio en consola
        except:
            print("No se pudo cambiar de directorio o no existe el directorio seleccionado")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("No se pudo cambiar de directorio o no existe el directorio seleccionado")
            sh.write(error + " " "\n")
            sh.close()
    # Comando para agregar usuario 
    def do_agguser(self, inp):
        self.logshell("agguser", inp)
        inp = inp.split()
        try:
            # Realiza una llamada a la funcion useradd con os
            # el horario de entrada y salida :/ y por ultimo el ip | si no se sabe la ip introducir ip addr
            sh = open("/var/log/usuarios_log", "a")
            user = str.format(inp[0] + " " + inp[1] + " " + inp[2] + " " + inp[3] + " ")
            sh.write(user + " " "\n")
            sh.close()
            os.system("useradd  " + inp[0])
            sh =open("/root/usuarios_log", "a")
            user1 = str.format(inp[0] + " ")
            sh.write(user1 + " " "\n")
            sh.close()
        except:
            print("No se pudo crear el usuario o se introdujo algun caracter no permitido")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("No se pudo crear el usuario o se introdujo algun caracter no permitido ")
            sh.write(error + " " "\n")
            sh.close()
    #Comando para iniciar el archivo sh que contiene un loop que imprime la fecha, luego mediante subprocesos, los subprocesos se crearan y obtendran un PID
    #entonces en cada loop se imprimira la fecha acompañado con el subproceso iniciado con su PID       
    def do_iniciar(self, inp):
        #recibe el nombre del archivo sh a ejecutar
        global pids #variable global para alojar los subprocesos PIDS
        self.logshell("iniciar",inp)  # llamaa a la funcion logshell y escribe rename y lo escrito en teclado para saber los parametros
        inp = inp.split()
        try:
            shellscript = subprocess.Popen([inp[0]], stdin=subprocess.PIPE) #el subproceso abre el archivo sh para tomarlo como un nuevo proceso, stdin lo enumera
            #y lo guarda en shellscript
            pids[str(shellscript.pid)] = shellscript #shellscript es un objeto que representa al proceso, y se guarda en pids, usamos str para poder convertir a objeto
            print("Subproceso iniciado, PID: "+str(shellscript.pid)) #imprime el subproceso con su PID
        except:
            print("Error, verifique si esta bien el nombre del archivo Sh")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("Error, verifique si esta bien el nombre del archivo Sh")
            sh.write(error + " " "\n")
            sh.close()    
    
    #Comando para matar los PID procesos (DEMONIOS)    
    def do_matar(self, inp):
        #recibe el pid del proceso a matar
        global pids
        self.logshell("matar",inp)  # llamaa a la funcion logshell y escribe rename y lo escrito en teclado para saber los parametros
        inp = inp.split()
        try:
            shellscript = pids[inp[0]] #el numero que se ingreso en teclado se guarda de nuevo en shellscript
            if shellscript is not None: #si es real imprime el print y mata con kill()
                print("Matando proceso numero: "+inp[0])
                shellscript.kill()
        except:
            print("Error, verifique si el PID es correcto")
            sh = open("/var/log/errores_sistema_log", "a")
            error = str.format("Error, verifique si el PID es correcto")
            sh.write(error + " " "\n")
            sh.close()       

    # Comando para salir de la shell
    def do_exit(self, inp):
        global usuario   #traemos el usuario que habia sido guardado en global usuario para saber quien es 
        sh = open("/var/log/personal_horarios_log", "a") #iregistrara la hora que cerro sesion en el archivo personal_horarios_log
        sh.write(datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + " El usuario" + usuario + " ha salido: \n")#con esta funcion registra la hora actual para poder copiar en el archivo
        sh.close()
        print("Tu has salido de la shell")
        return True



    # Ayuda que se imprime en la pantalla al escribir help (+comando)
    # Muestra la ayuda correspondiente a cada comando
    def help_exit(self):
        print('para salir de la aplicacion escriba exit')


    def help_mover(self):
        print('\nUso  del comando mover:\n')
        print('\t\tmover [archivo] [directoriodestino]\n')


    def help_renom(self):
        print('\nUso del comando renom:\n')
        print('\t\trenom [archivo1]  [archivo2]\n')


    def help_crdir(self):
        print('\nUso del comando crdir:\n')
        print('\t\tcrdir [directorio]\n')


    def help_cprop(self):
        print('\nUso del comando cprop:\n')
        print('\t\tcprop [archivo|directorio]  usuario  grupo\n')


    def help_cambiarper(self):
        print('\nUso del comando cambiarper:\n')
        print('\t\tcambiarper [archivo] [tipo de permiso en octal]\n')


    def help_cambpass(self):
        print('\nUso del comando cambpass:\n')
        print('\t\tcambpass\nNew password:[NUEVA CONTRASEÑA]\nConfirm password:[NUEVA CONTRASEÑA]\n')


    def help_cop(self):
        print('\nUso del comando cop:\n')
        print('\t\tcop [archivoEntrada] [directorio + archivo]\n')
        print('\t\tEjemplo: archivo.txt /root/carpeta/archivo.txt \n')


    def help_listar(self):
        print('\nUso del comando listar:\n')
        print('\t\tlistar [directorio]\n')


    def help_cambiardir(self):
        print('\nUso del comando cambiardir:\n')
        print('\t\tcambiardir [directorio]\n')


    def help_agguser(self):
        print('\nUso del comando agguser:\n')
        print('\t\tagguser [usuario] [hh-mm] [hh-mm] ip ]\n')

    def help_login(self):
        print('\nUso del comando login para registar horarios de entrada y salida:\n')
        print('\t\tlogin [usuario]  \n')

    def help_scp(self):
        print('\nUso del comando scptransfer:\n')
        print('\t\tscp -S arhivo usuario@hostname:LugardeGuardado \n')

    def help_demonios(self):
        print('\nUso del comando para iniciar y apagar demonios :\n')
        print('\t\t iniciar [nombre archivo.sh]\n')
        print('\t\t Luego para apagar/matar el proceso -->  matar [PID]\n')


    #ejecutar todos los comandos del sistema del bash
    def default(self, inp):
        # llama a las funciones que la shell no tiene declaradas
        subprocess.run(inp, shell=True, executable="/bin/bash")
        if inp.split()[0] == "scp": #si lo escrito fue scp, entonces llama a la funcion logshell para guardar en la bitacora
            self.logshell_transferencias(inp)  # llamaa a la funcion logshell y escribe la bitacora de las transferencias 

      

    do_EOF = do_exit  #Se ejecuta la funcion para salir del sistema
    help_EOF = help_exit

MyPrompt().cmdloop()   #Para continuar recibiendo ordenes infinitamente
